<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CAS</title>
<style>
  body {
    color: #606c76;
    font-family: 'Roboto', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;
    font-size: 1.6em;
    font-weight: 300;
    letter-spacing: .01em;
    line-height: 1.6;
    background-color: #e7e7e7;
  }

  .container {
    margin: 0 auto;
    max-width: 112.0rem;
    padding: auto;
    position: relative;
    width: 100%;
  }

  .row {
    display: flex;
    flex-direction: column;
    padding: 0;
    width: 100%;
    margin: auto;
  }

  .box {
    -webkit-transition: 0.3s box-shadow ease;
    flex: 0 0 50%;
    max-width: 50%;
    background-color: white;
    border-radius: 5px;
    padding: auto;
    padding: 20px;
    margin: 5em auto auto auto;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  .box:hover {
    box-shadow: 0 12px 23px rgba(0, 0, 0, 0.23), 0 10px 10px rgba(0, 0, 0, 0.19);
  }

  h3,
  h4,
  h5 {
    font-weight: 300;
    letter-spacing: -.1rem;
    margin-bottom: 1.0rem;
    margin-top: 0;
  }

  h3 {
    font-size: 2.8rem;
    line-height: 1.3;
  }

  h4 {
    font-size: 2.2rem;
    letter-spacing: -.08rem;
    line-height: 1.35;
  }

  h5 {
    font-size: 1rem;
    letter-spacing: -.05rem;
    line-height: 1.5;
  }

  a {
    color: white;
    text-decoration: none;
    background-color: #3C53B2;
    padding: 0.1em 2em 0.1em 2em;
    border-radius: 50px;
  }

  a:focus, a:hover {
    color: #3C53B2;
    background-color: #C4CBE7;
  }

</style>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
      $()
    })
</script>
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="box">
        <h3>Pemfung-pemfungan</h3>
        <h5>Click the button below to login</h5>
        <a href="{{ location }}">Log In</a>
      </div>
    </div>
  </div>
</body>

</html>