%% @author valen
%% @copyright 2017 valen
%% Generated on 2017-12-06
%% @doc This site was based on the 'blog' skeleton.

%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(castest).
-author("pemfung").

-mod_title("CAS Client authentication").
-mod_description("Provides integration to any CAS Server for zotonic application").
-mod_prio(400).

-include_lib("zotonic.hrl").



%%====================================================================
%% support functions go here
%%====================================================================

