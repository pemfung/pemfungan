%% @author author <author@example.com>
%% @copyright YYYY author.
%% @doc Example contact-form handler.

-module(controller_cas_2).

-include_lib("controller_html_helper.hrl").
-include_lib("xmerl/include/xmerl.hrl").

-define(HOST, "http://casserverpac4j.herokuapp.com").
-define(SERVICE, "http://castest.dev:8000/cas_test_2").

html(Context) ->
    case z_context:get_q(<<"ticket">>, Context) of
		undefined ->
 			Ticket=undefined;		
		Ticket ->
			ok
		end,
		case Ticket of
			undefined ->
				Location=?HOST
		                ++ "/login?service="
		                ++ z_url:url_encode(?SERVICE),

				% {{redirect, Location}, Context};
				Html = z_template:render("cas_test.tpl", [{location, Location}], Context),
						z_context:output(Html, Context);

			Ticket ->
				Request_url=?HOST
		                ++ "/serviceValidate?ticket="
		                ++ z_url:url_encode(Ticket)
		                ++ "&service="
		                ++ z_url:url_encode(?SERVICE),
				{ok, {_, _, Response}} = httpc:request(Request_url),
				case string:str(Response, "authenticationFailure") > 0 of
					true ->
						Html = z_template:render("failure.tpl", [{ticket, Ticket}, {response, Response}], Context),
						z_context:output(Html, Context);
					false ->
						Logout_url=?HOST
			                ++ "/logout?service="
			                ++ z_url:url_encode(?SERVICE),
						Html = z_template:render("success.tpl", [{response, Response}, 
							{logout_url, Logout_url}], Context),
						z_context:output(Html, Context)
				end
		end.
