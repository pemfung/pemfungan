<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CAS</title>
<style>
    .codebox {
        /* Below are styles for the codebox (not the code itself) */
        border:1px solid black;
        background-color:#EEEEFF;
        overflow:auto;
        padding:10px;
        margin-bottom: 1em;
    }
    .codebox code {
        /* Styles in here affect the text of the codebox */
        font-size:0.7em;
        /* You could also put all sorts of styling here, like different font, color, underline, etc. for the code. */
    }

    body {
      color: #606c76;
      font-family: 'Roboto', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;
      font-size: 1.6em;
      font-weight: 300;
      letter-spacing: .01em;
      line-height: 1.6;
      background-color: #e7e7e7;
    }

    .container {
      margin: 0 auto;
      max-width: 112.0rem;
      padding: auto;
      position: relative;
      width: 100%;
    }

    .row {
      display: flex;
      flex-direction: column;
      padding: 0;
      width: 100%;
    }

    .box {
      -webkit-transition: 0.3s box-shadow ease;
      flex: 0 0 50%;
      max-width: 50%;
      background-color: white;
      border-radius: 5px;
      padding: auto;
      padding: 20px;
      margin-top: 5em;
      margin-left: 25%;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    }

    h1,
    h5 {
      font-weight: 300;
      letter-spacing: -.1rem;
      margin-bottom: 1.0rem;
      margin-top: 0;
    }

    h1 {
      font-size: 4.6rem;
      line-height: 1.2;
    }

    h5 {
      font-size: 1.2rem;
      letter-spacing: -.05rem;
      line-height: 1.5;
    }

    a {
      color: white;
      text-decoration: none;
      background-color: #3C53B2;
      padding: 0.1em 2em 0.1em 2em;
      border-radius: 50px;
    }

    a:focus, a:hover {
      color: #3C53B2;
      background-color: #C4CBE7;
    }
</style>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
        function formatXml(xml) {
            var formatted = '';
            var reg = /(>)(<)(\/*)/g;
            xml = xml.replace(reg, '$1\s$2$3');
            var pad = 0;
            jQuery.each(xml.split('\n'), function(index, node) {
                var indent = 0;
                if (node.match( /.+<\/\w[^>]*>$/ )) {
                    indent = 0;
                } else if (node.match( /^<\/\w/ )) {
                    if (pad != 0) {
                        pad -= 1;
                    }
                } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {
                    indent = 1;
                } else {
                    indent = 0;
                }
                var padding = '';
                for (var i = 0; i < pad; i++) {
                    padding += '  ';
                }
                console.log("node :" + node);
                if(!node.match(/^\s*$/)){
                	formatted += padding + node + '\n';
                	pad += indent;	
                }
                
            });
            return formatted;
        }
        xml_raw = `{{response}}`;
        xml_formatted = formatXml(xml_raw);
        xml_escaped = xml_formatted.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/ /g, '&nbsp;').replace(/\n/g,'<br />');
        $('.box_1').html(xml_escaped);
    })
</script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="box">
        <h1>Failure</h1>
        <h5>Here's the response given from server</h5>
        <div class="codebox">
            <code class="box_1">
            </code>
        </div>
      </div>
    </div>
  </div>
</body>
</html>